﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{
    private Rigidbody2D _rb2;
    private float moveSpeed = 10;

    private void Awake()
    {
        _rb2 = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        
    }

    private void FixedUpdate()
    {
        TouchMove();
    }

    public void TouchMove()
    {
        if (Input.GetMouseButton(0) & Camera.main != null)
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (touchPos.x < 0)
            {
                // move to left
                _rb2.velocity = Vector2.left * moveSpeed;
            }
            else
            {
                // move to right 
                _rb2.velocity = Vector2.right * moveSpeed;
            }
        }
        else
        {
            _rb2.velocity = Vector2.zero;
        }
    }
}
