﻿using UnityEngine;
using Random = UnityEngine.Random;

public class Ball : MonoBehaviour
{
    private Rigidbody2D _rb2;
    public float bounceForce;
    private bool _gameStarted;
    private bool _firstTimeBouncing = true;
    

    private void Awake()
    {
        _rb2 = GetComponent<Rigidbody2D>();
    }
    

    // Update is called once per frame
    void Update()
    {
        if (!(Input.anyKeyDown & !_gameStarted)) return;
        _rb2.gravityScale = 1;
        GameManager.Instance.GameStart();
        _gameStarted = true;
    }

    private void StartBounce()
    {
        var randomNum = Random.Range(-1, 1);
        while (randomNum == 0)
        {
            randomNum = Random.Range(-1, 1);
        }
        var randomDirection = new Vector2(randomNum, 1);
        
        _rb2.AddForce(randomDirection * bounceForce);
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag($"FallCheck"))
        {
            GameManager.Instance.Restart();
        }
        else if (col.gameObject.CompareTag($"Paddle"))
        {
            GameManager.Instance.ScoreUp();
            if (_firstTimeBouncing)
            {
                StartBounce();
                _firstTimeBouncing = false;
            }
        }
    }
}
